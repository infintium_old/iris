package iris

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"net"

	log "github.com/sirupsen/logrus"
)

type Client struct {
	UpdateSrvAddr string
	session       *Session
	gs            *gobClient
	mux           SessionMux
	ctx           *log.Entry
}

func Connect(conf *tls.Config, addr, uaddr string) (*Client, error) {
	var c Client

	// taddr, err := net.ResolveTCPAddr("tcp", addr)
	// if err != nil {
	// 	return nil, err
	// }

	// log.Info("Resolved TCP addr")

	conn, err := tls.Dial("tcp", addr, conf)
	if err != nil {
		return nil, err
	}

	c.ctx = log.WithField("addr", conn.RemoteAddr().String())

	laddr := conn.LocalAddr()
	m := muxListener{addr: laddr.(*net.TCPAddr)}

	m.c = make(chan net.Conn)

	c.mux.listeners = make(map[StreamType]muxListener)
	c.mux.listeners[Pty] = m

	go PtyServe(&m)

	c.session, err = newClientSession(conn, &c.mux, c.ctx)
	if err != nil {
		return nil, err
	}

	// le := log.WithField("addr", session.RemoteAddr().String())
	//le = le.WithField("name", session.ConnectionState().PeerCertificates[0].Subject.CommonName)

	c.ctx.Info("Connected")

	c.gs, err = c.session.OpenGobStream(c.ctx)
	if err != nil {
		return nil, err
	}

	// Open connection to update server
	go c.session.OpenTcpStream(uaddr, c.ctx)

	return &c, nil
}

func (c *Client) Upload(local, remote string) error {

	return c.gs.upload(local, remote)
}

func (c *Client) Close() {
	c.session.Close(c.ctx)
}

func LoadClientConfig(ca, crt, key string) (*tls.Config, error) {
	caCertPEM, err := ioutil.ReadFile(ca)
	if err != nil {
		return nil, err
	}

	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM(caCertPEM)
	if !ok {
		log.Fatal("Failed to parse root certificate")
	}

	cert, err := tls.LoadX509KeyPair(crt, key)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      roots,
		CipherSuites: []uint16{tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305},
	}, nil
}
