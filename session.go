package iris

import (
	"io"
	"net"
	"time"

	"bitbucket.org/infintium/smux"
	log "github.com/sirupsen/logrus"
)

type muxListener struct {
	c    chan net.Conn
	addr *net.TCPAddr
}

func (m *muxListener) Accept() (net.Conn, error) {
	c := <-m.c
	return c, nil
}

func (m *muxListener) Close() error {
	close(m.c)
	return nil
}

func (m *muxListener) Addr() net.Addr {
	return m.addr
}

type SessionMux struct {
	listeners map[StreamType]muxListener
}

type Session struct {
	*smux.Session
	mux *SessionMux
	ctx *log.Entry
	die <-chan struct{}
}

func newServerSession(conn io.ReadWriteCloser, mux *SessionMux, ctx *log.Entry) (*Session, error) {
	session, err := smux.Server(conn, nil)
	if err != nil {
		return nil, err
	}
	// defer session.Close()

	log.Info("Session accepted (Connect)")

	s := &Session{
		Session: session,
		mux:     mux,
		ctx:     ctx,
		die:     session.CloseChan(),
	}

	// TODO: Do we want this here?
	go s.serve()

	return s, nil
}

func newClientSession(conn io.ReadWriteCloser, mux *SessionMux, ctx *log.Entry) (*Session, error) {
	// cfg := smux.DefaultConfig()
	// cfg.LogOutput = le.Logger.Writer()

	session, err := smux.Client(conn, nil)
	if err != nil {
		return nil, err
	}
	// defer session.Close()

	ctx.Info("Session accepted (Connect)")

	s := &Session{
		Session: session,
		mux:     mux,
		ctx:     ctx,
		die:     session.CloseChan(),
	}

	go s.serve()

	return s, nil
}

func (s *Session) AcceptStream() (*Stream, error) {
	stream, err := s.Session.AcceptStream()
	if err != nil {
		return nil, err
	}

	t := StreamType(stream.UID())

	ctx := s.ctx.WithField("id", stream.ID())
	ctx = ctx.WithField("type", t)

	ctx.Info("Accepted stream")

	return &Stream{Stream: stream, st: t, ctx: ctx}, nil
}

func (s *Session) serve() {
	// defer s.Close()
	for {
		stream, err := s.AcceptStream()
		if err != nil {
			if s.Session.IsClosed() == true {
				s.ctx.Info("Session closed (", err, ")")
			} else {
				s.ctx.Error("Accept stream error: ", err)
				s.Close(s.ctx)
			}
			return
		}

		cc := ContextConn{Conn: stream, ctx: stream.ctx}

		if s.mux == nil {
			stream.ctx.Error("No listeners registered for session")
			stream.Close()
		} else {
			m, ok := s.mux.listeners[stream.st]
			if ok == true {
				m.c <- cc
			} else {
				stream.ctx.Error("No listener registered for type ", stream.st)
			}
		}
	}

	s.Close(s.ctx)
}

func (s *Session) OpenTcpStream(addr string, le *log.Entry) error {
	// TODO: Do we need this special case of TCPListener? (i.e. keepAlive?)
	l, err := TCPListener(addr)
	if err != nil {
		return err
	}

	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			return err
		}

		go func() {
			stream, err := s.Session.OpenStream(uint16(Tcp))
			if err != nil {
				return
			}

			le = le.WithField("id", stream.ID())

			le.Info("TCP stream open")

			err = transport(stream, conn)

			if err != nil && err != io.EOF {
				le.Error("Error stream handle: ", err)
			}

			le.Info("Closing stream")

			conn.Close()
			stream.Close()
		}()
	}

	return nil
}

func (s *Session) OpenPtyStream(addr string, le *log.Entry) error {
	l, err := net.Listen("unix", addr)
	if err != nil {
		return err
	}

	defer func() {
		err := l.Close()
		if err != nil {
			le.Error("Error closing pty listener: ", err)
		} else {
			le.Info("Closed pty listener: ", err)
		}
	}()

	newConns := make(chan net.Conn)

	go func(ln net.Listener) {
		for {
			c, err := ln.Accept()
			if err != nil {
				// handle error (and then for example indicate acceptor is down)
				newConns <- nil
				return
			}
			newConns <- c
		}
	}(l)

	for {
		select {
		case c := <-newConns:
			if c == nil {
				return nil
			}
			go func() {
				stream, err := s.Session.OpenStream(uint16(Pty))
				if err != nil {
					return
				}

				le = le.WithField("id", stream.ID())
				le = le.WithField("type", StreamType(stream.UID()))

				le.Info("Stream opened")

				err = transport(stream, c)

				if err != nil && err != io.EOF {
					le.Error("Error stream handle: ", err)
				}

				// TODO: Check errors
				c.Close()
				stream.Close()

				le.Info("Stream closed")
			}()
		case <-s.die:
			return nil
		}
	}

	return nil
}

func (s *Session) OpenGobStream(le *log.Entry) (*gobClient, error) {
	stream, err := s.Session.OpenStream(uint16(Gob))
	if err != nil {
		return nil, err
	}

	le = le.WithField("id", stream.ID())

	le.Info("Stream open")

	gc := GobClient(stream)

	return gc, nil
}

func (s *Session) Close(le *log.Entry) {
	if !s.Session.IsClosed() {
		err := s.Session.Close()
		if err != nil {
			le.Error("Close error: ", err)
		}

		le.Info("Session closed")
	}
}

func UnixListener(addr string) (*net.UnixListener, error) {
	laddr, err := net.ResolveUnixAddr("unix", addr)
	if err != nil {
		return nil, err
	}

	ln, err := net.ListenUnix("unix", laddr)
	if err != nil {
		return nil, err
	}

	return ln, nil
}

// Listener is a proxy server listener, just like a net.Listener.
type Listener interface {
	net.Listener
}

type tcpListener struct {
	net.Listener
}

// TCPListener creates a Listener for TCP proxy server.
func TCPListener(addr string) (Listener, error) {
	laddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}
	ln, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		return nil, err
	}
	return &tcpListener{Listener: tcpKeepAliveListener{ln}}, nil
}

type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	tc, err := ln.AcceptTCP()
	if err != nil {
		return
	}
	tc.SetKeepAlive(true)
	tc.SetKeepAlivePeriod(180 * time.Second)
	return tc, nil
}
