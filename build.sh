#!/bin/sh

VERSION=$1

if [[ -z $VERSION ]]; then
    VERSION="dev"
else
    [[ $VERSION != v+([0-9]).+([0-9]).+([0-9]) ]] &&
    {
        echo "Incorrect version format. Use vX.Y.Z"
        exit
    }
fi

export GOARCH=amd64

go version
echo -n "GOARCH:   "
go env GOARCH
echo -n "GOOS:     "
go env GOOS

COMMIT=$(git rev-parse HEAD)
STAMP=$(date --iso-8601=seconds)

echo "Version: " ${VERSION}
echo "Commit:  " ${COMMIT}
echo "Stamp:   " ${STAMP}

CC=/usr/local/musl/bin/musl-gcc go build -ldflags "-linkmode external -extldflags '-static' -X main.version=${VERSION} -X main.commit=${COMMIT} -X main.stamp=${STAMP}" cmd/iris-server/iris-server.go
CC=/usr/local/musl/bin/musl-gcc go build -ldflags "-linkmode external -extldflags '-static' -X main.version=${VERSION} -X main.commit=${COMMIT} -X main.stamp=${STAMP}" cmd/iris-term/iris-term.go
