package iris

import (
	"encoding/gob"
	"io"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"syscall"

	"bitbucket.org/infintium/smux"
	"github.com/kr/pty"

	log "github.com/sirupsen/logrus"
)

type StreamAddr struct {
	stype      StreamType
	addr, host string
}

func (s *StreamAddr) Network() string {
	return s.stype.String()
}

func (s *StreamAddr) String() string {
	return s.addr
}

type Stream struct {
	*smux.Stream
	st  StreamType
	ctx *log.Entry
}

type ContextConn struct {
	net.Conn
	ctx *log.Entry
}

type StreamHandler interface {
	Handle(*smux.Stream, *log.Entry) error
}

func init() {
	gob.Register(upload{})
}

func transport(a, b io.ReadWriter) error {
	errc := make(chan error, 1)

	go func() {
		_, err := io.Copy(a, b)
		errc <- err
	}()

	go func() {
		_, err := io.Copy(b, a)
		errc <- err
	}()

	err := <-errc

	return err
}

type gobServer struct {
	root string
}

func GobServer(root string) *gobServer {
	// TODO: Make sure folder exists
	return &gobServer{root: root}
}

func GobServe(l net.Listener, gs *gobServer) error {
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error(err)
			continue
		}

		go func() {
			d := gob.NewDecoder(conn)

			var ctx *log.Entry
			cc, ok := conn.(ContextConn)
			if ok {
				ctx = cc.ctx
			} else {
				ctx = log.WithField("addr", conn.RemoteAddr().String())
			}

			var rx interface{}
			for {
				err := d.Decode(&rx)
				if err != nil {
					if err != io.EOF {
						ctx.Error("Error decoding: ", err)
					}
					break
				}

				switch v := rx.(type) {
				case upload:
					ctx.Infof("Upload '%s %d %x'\n", v.Name, len(v.Data), v.Hash)

					err = v.checkAndWrite(gs.root)
					if err != nil {
						ctx.Error("Error with file: ", err)
					}
				default:
					ctx.Error("Unrecognized gob type")
				}
			}

			err = conn.Close()
			if err != nil {
				ctx.Error("Error closing stream: ", err)
			}

			ctx.Info("Stream closed")
		}()

	}
}

type gobClient struct {
	e *gob.Encoder
}

func GobClient(s *smux.Stream) *gobClient {
	return &gobClient{e: gob.NewEncoder(s)}
}

func (gc *gobClient) upload(local, remote string) error {
	var u interface{}
	var err error

	u, err = readAndSum(local, remote)
	if err != nil {
		return err
	}

	err = gc.e.Encode(&u)
	if err != nil {
		return err
	}

	return nil
}

func PtyServe(l net.Listener) error {
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error(err)
			continue
		}

		go func() {
			var ctx *log.Entry
			cc, ok := conn.(ContextConn)
			if ok {
				ctx = cc.ctx
			} else {
				ctx = log.WithField("addr", conn.RemoteAddr().String())
			}

			defer func() {
				err = conn.Close()
				if err != nil {
					ctx.Error("Error closing stream: ", err)
				}

				ctx.Info("Stream closed")
			}()

			c := exec.Command("/bin/sh", "--login")

			ptmx, err := pty.Start(c)
			if err != nil {
				ctx.Error("Error starting pty: ", err)
				return
			}

			// Make sure to close the pty at the end.
			defer func() { _ = ptmx.Close() }() // Best effort.

			// Handle pty size.
			ch := make(chan os.Signal, 1)
			signal.Notify(ch, syscall.SIGWINCH)
			go func() {
				for range ch {
					if err := pty.InheritSize(os.Stdin, ptmx); err != nil {
						ctx.Printf("error resizing pty: %s", err)
					}
				}
			}()
			ch <- syscall.SIGWINCH // Initial resize.

			// FIXME: This will error when server closes pty
			err = transport(conn, ptmx)
			if err != nil {
				ctx.Error("Error with transport: ", err)
			}
		}()

	}
}
