package iris

import (
	"bytes"
	"compress/gzip"
	"encoding/gob"
	"io"
)

type CANMsg struct {
	Time int64  // Timestamp in Unix Nanoseconds
	ID   uint32 // 32 bit CAN_ID + EFF/RTR/ERR flags
	//DLC  uint8    // frame payload length in byte (0 .. 8)
	Data []uint8 // raw data
}

type CANMsgEncoder struct {
	io.Writer
	b  *bytes.Buffer
	g  *gzip.Writer
	w  *WriteCounter
	e  *gob.Encoder
	ts int64
}

func NewCANMsgEncoder(w io.Writer) *CANMsgEncoder {
	e := &CANMsgEncoder{Writer: w}

	e.b = new(bytes.Buffer)

	e.g, _ = gzip.NewWriterLevel(e.b, gzip.BestCompression)

	e.w = NewWriteCounter(e.g)

	e.e = nil

	return e
}

func (e *CANMsgEncoder) Encode(msg CANMsg) error {
	if e.e == nil {
		e.e = gob.NewEncoder(e.w)
		e.ts = 0
	}

	rel := msg.Time - e.ts

	if rel < 30000 {
		msg.Time = rel
	} else {
		e.ts = msg.Time
	}

	return e.e.Encode(msg)
}

func (e *CANMsgEncoder) Flush() (in, out int64, err error) {
	if e.e != nil {
		in = e.w.Len()

		e.w.Reset()

		if err = e.g.Close(); err != nil {
			return
		}

		if out, err = e.b.WriteTo(e.Writer); err != nil {
			return
		}

		e.b.Reset()

		e.g.Reset(e.b)

		e.e = nil
	}

	return
}

type WriteCounter struct {
	io.Writer
	n int64
}

func NewWriteCounter(w io.Writer) *WriteCounter {
	return &WriteCounter{Writer: w, n: 0}
}

func (w *WriteCounter) Write(p []byte) (n int, err error) {
	n, err = w.Writer.Write(p)
	w.n += int64(n)
	return
}

func (w *WriteCounter) Len() int64 {
	return w.n
}

func (w *WriteCounter) Reset() {
	w.n = 0
}
