# IRIS

Integrated Remote Inter-connectivity System

## Description

A secure system to connect field units to a central server.

## Notes

Certificate research: <https://crypto.stackexchange.com/questions/42621/best-choice-out-of-these-six-tls-cipher-suites>

- Symertic cipher: ChaCha20 / Poly1305 better for embedded (i.e. 32-bit or no hardware support)
- Aysmmetic key exchange: ECDHE (Supported by Go)
- Certificate signing: ECDSA, use smaller key for speed (prime256v1 seems to be fastest on x86-64)
- Go may not have good ARM support for other methods: <https://github.com/golang/go/issues/19840>

Remote pty example: <https://github.com/StalkR/misc/tree/master/pty>
Tunnel multiple protocols: <https://github.com/ginuerzh/gost>

## TODO

-----------

- Reverse tunnel for terminal:
  - Create unix socket stream type
    - Server creates unix socket for each connected client
    - Client waits for connection then creates pty
    - Need 3rd program to connect socket with terminal
- Clean up log.Entry and error returns
  - How to log close()?
- Remote update tunnel
  - TCP tunnel functional but can only have 1 per session
  - Could re-work smux ID scheme; ID == type.port instead of type.sequential
- Client:
  - Client should connect TLS then connect Session and return (Client hold Session?)
  - Request new GobStream from Session -> OpenGobStream
- Fix logging:
  - Can we get away from passing log entries around?
- systemd integration for server
- Upload() check for return error
- Influx timed flush
- More controlled termination / shutdown
- Speed tests?
- Determine which TCP port to use
- Setup build scripts / linker vars
- Setup vendor deps
- Clean up and comments

## TESTING

-----------

- Look into golang unit tests
- Verify appropriate logging
- Certificate testing
  - Wrong  cert
  - Wrong cert cipher
  - Revoked cert

## Key Generation

-----------

- Reference: <https://gist.github.com/QueuingKoala/e2c1c067a312384915b5>
- Use easy-rsa >= v3.0.4 (Random serial number generation) <https://github.com/OpenVPN/easy-rsa>
- Copy & edit /usr/share/doc/easy-rsa/vars.example:
  set_var EASYRSA_ALGO            ec
  set_var EASYRSA_CURVE           prime256v1

```bash
# Setup path
export PATH=$PATH:/usr/share/easy-rsa/3/

# Build root CA (on air-gapped machine):
EASYRSA_PKI=offline easyrsa init-pki
cp ./vars.example ./offline/vars
EASYRSA_PKI=offline easyrsa build-ca nopass

# Build sub-CA request
EASYRSA_PKI=sub easyrsa init-pki
cp ./vars.example ./sub/vars
EASYRSA_PKI=sub easyrsa build-ca nopass subca

# Import the sub-CA request under the short-name "sub" on the offline PKI:
EASYRSA_PKI=offline easyrsa import-req sub/reqs/ca.req sub
# Then sign it as a CA:
EASYRSA_PKI=offline easyrsa sign-req ca sub
# Transport sub-CA cert to sub PKI:
cp offline/issued/sub.crt sub/ca.crt

# Use the following to edit DN before cert generation
EASYRSA_PKI=sub easyrsa gen-req client0 nopass
EASYRSA_PKI=sub easyrsa sign-req client0

# Use the follwing to generate cert using default values in vars
EASYRSA_PKI=sub easyrsa build-server-full localhost nopass
EASYRSA_PKI=sub easyrsa build-client-full client0 nopass

# Build a client, revoke and update CRL
EASYRSA_PKI=sub easyrsa build-client-full badClient nopass
EASYRSA_PKI=sub easyrsa revoke badClient
EASYRSA_PKI=sub easyrsa gen-crl
```
