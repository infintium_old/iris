package iris

import (
	"bufio"
	"errors"
	"hash/crc32"
	"io/ioutil"
	"os"
	"path/filepath"
)

func readAndSum(local, remote string) (*upload, error) {
	var u upload
	f, err := os.Open(local)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	u.Name = remote

	if u.Data, err = ioutil.ReadAll(f); err != nil {
		return nil, err
	}

	u.Hash = crc32.Checksum(u.Data, crc32c)

	// log.Debugf("Checksum: %x", u.Hash)

	return &u, nil
}

func (u *upload) checkAndWrite(path string) error {
	crc := crc32.Checksum(u.Data, crc32c)

	// log.Debug("Verify '%x'\n", sum)

	if crc != u.Hash {
		return errors.New("Checksum failed")
	}

	f, err := os.Create(filepath.Join(path, filepath.Clean(u.Name)))
	if err != nil {
		return err
	}
	defer f.Close()

	b := bufio.NewWriter(f)
	_, err = b.Write(u.Data)
	if err != nil {
		return err
	}

	err = b.Flush()
	if err != nil {
		return err
	}

	err = f.Sync()
	if err != nil {
		return err
	}

	return nil
}
