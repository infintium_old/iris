package main

import (
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	"bitbucket.org/infintium/iris"
	"github.com/BurntSushi/toml"
	influx "github.com/influxdata/influxdb/client/v2"
	log "github.com/sirupsen/logrus"
)

const addr = "localhost:4242"

type client struct {
	DB   string            `toml:"db"`
	Tags map[string]string `toml:"tags"`
}

type config struct {
	ListenAddr string            `toml:"listen_addr"`
	CACrt      string            `toml:"ca_crt"`
	ServerCrt  string            `toml:"server_crt"`
	ServerKey  string            `toml:"server_key"`
	CRL        string            `toml:"crl"`
	InfluxAddr string            `toml:"influx_addr"`
	UploadRoot string            `toml:"upload_root"`
	UpdateRepo string            `toml:"update_repo"`
	Clients    map[string]client `toml:"clients"`
}

// Passed during building of program:
// go build -ldflags "-X main.buildDate=`date -u +%Y%m%d.%H%M%S`"
var (
	version string
	commit  string
	stamp   string
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: iris-server <config>")
	}

	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})

	log.Infoln("Build Version:", version)
	log.Infoln("Build Commit: ", commit)
	log.Infoln("Build Stamp:  ", stamp)

	var cfg config
	_, err := toml.DecodeFile(os.Args[1], &cfg)
	if err != nil {
		log.Fatal("Error reading config:", err)
	}

	log.Info(cfg.Clients)

	tlsCfg, err := iris.LoadServerConfig(cfg.CACrt, cfg.ServerCrt, cfg.ServerKey, cfg.CRL)
	if err != nil {
		log.Fatal(err)
	}

	log.Info("Loaded Server config")

	inflx, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr: cfg.InfluxAddr,
		//Username: username,
		//Password: password,
		UserAgent: "iris",
	})
	if err != nil {
		log.Fatal("Error connecting to Influx database")
	}

	log.Info("Connected to influx at ", cfg.InfluxAddr)

	// Create an Influx batch for each client
	db := make(map[string]*iris.Batch)
	for sn, c := range cfg.Clients {
		log.Debugln("Creating batch:", sn, c.DB, c.Tags)
		db[sn], err = iris.NewBatch(inflx, c.DB, "ms", "iris", c.Tags, 1000)
		if err != nil {
			log.Fatal("Could not create batch: ", err)
		}
	}

	laddr, err := net.ResolveTCPAddr("tcp", cfg.ListenAddr)
	if err != nil {
		log.Fatal("Could not resolve address: ", err)
	}

	sockpath := filepath.Clean("/tmp/iris-server")
	os.MkdirAll(sockpath, os.ModePerm)

	s := iris.Server{Addr: laddr, TLSConfig: tlsCfg, DB: db, SockPath: sockpath}

	gobListener := s.MuxListen(iris.Gob)
	go iris.GobServe(gobListener, iris.GobServer(cfg.UploadRoot))

	httpListener := s.MuxListen(iris.Tcp)
	go http.Serve(httpListener, http.FileServer(http.Dir(cfg.UpdateRepo)))

	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		err = s.Close(log.WithField("none", 0))
		if err != nil {
			log.Fatal(err)
		}
		os.Exit(1)
	}()

	err = s.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}

}
