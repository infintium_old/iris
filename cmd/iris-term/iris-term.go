package main

import (
	"fmt"
	"io"
	"net"
	"os"

	"golang.org/x/crypto/ssh/terminal"
)

// Passed during building of program:
// go build -ldflags "-X main.buildDate=`date -u +%Y%m%d.%H%M%S`"
var (
	version string
	commit  string
	stamp   string
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: iris-terminal <socket>")
		os.Exit(1)
	}

	fmt.Println("Build Version:", version)
	fmt.Println("Build Commit: ", commit)
	fmt.Println("Build Stamp:  ", stamp)

	conn, err := net.Dial("unix", os.Args[1])
	if err != nil {
		fmt.Println("Could not open socket: ", err)
		os.Exit(1)
	}
	defer conn.Close()

	// Set stdin in raw mode.
	oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}
	defer func() { _ = terminal.Restore(int(os.Stdin.Fd()), oldState) }() // Best effort.

	// Copy stdin to the pty and the pty to stdout.
	go func() { _, _ = io.Copy(conn, os.Stdin) }()
	_, _ = io.Copy(os.Stdout, conn)
}
