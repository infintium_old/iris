package main

import (
	"fmt"
	"io"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"bitbucket.org/infintium/iris"
	"github.com/BurntSushi/toml"
	log "github.com/sirupsen/logrus"
)

type config struct {
	Device     string
	Queue      string
	ServerAddr string `toml:"server_addr"`
	CACrt      string `toml:"ca_crt"`
	ClientCrt  string `toml:"client_crt"`
	ClientKey  string `toml:"client_key"`
	Timeout    int
	Filters    []iris.CANFilter
}

// Passed during building of program:
// go build -ldflags "-X main.buildDate=`date -u +%Y%m%d.%H%M%S`"
var (
	version string
	commit  string
	stamp   string
)

func readCANMsgs(sock *iris.CANSocket, msg chan iris.CANMsg) {
	for {
		cf, t, err := sock.ReadTimestampFrame()
		if err != nil {
			log.Fatal(err)
		}

		// fmt.Println(cf)

		m := iris.CANMsg{
			Time: t.UnixNano() / 100000,
			ID:   cf.ID,
			Data: make([]uint8, cf.DLC, cf.DLC),
		}

		copy(m.Data, cf.Data[0:cf.DLC])

		msg <- m
	}
}

func encodeCANMsgs(dq *iris.DiskQueue, msg chan iris.CANMsg, sig chan os.Signal, done chan bool) {
	// dql := NewLogWriter(dq, "diskq")

	e := iris.NewCANMsgEncoder(dq)

	i := 0

	timer := make(chan time.Time)

	for {

		select {

		case <-timer:
			in, out, err := e.Flush()
			if err != nil {
				log.Println("Error flushing encoder:", err)
			}
			log.Println("Encoded", in, "bytes to", out, "bytes")
			i = 0

		case m := <-msg:
			// fmt.Println("Encoding:", m)
			if err := e.Encode(m); err != nil {
				log.Println("Error encoding:", m, err)
				break
			}

			if i == 0 {
				time.AfterFunc(2*time.Second, func() { timer <- time.Now() })
			}
			i++

		case <-sig:
			in, out, err := e.Flush()
			if err != nil {
				log.Println("Error closing encoder:", err)
			}
			log.Println("Encoded", in, "bytes to", out, "bytes")

			done <- true
		}

		//fmt.Println("Encoded", i, "messages in", m, "blocks")
	}
}

func connectLoop() {
	// Get a connection
	for {
		start := time.Now()
		// client, err = simplessh.ConnectWithKeyTimeout(cfg.ServerAddr, "datalog", string(key), timeout)
		log.Info("Connecting...")
		client, err = iris.Connect(conf, cfg.ServerAddr, "127.0.0.1:8080")
		if err != nil {
			log.Warn("Couldn't establish a connection to the remote server ", err)
			time.Sleep(15 * time.Second)
		} else {
			timeElapsed(start, "Connect() complete")
			log.Info("Connected to the remote server ")
			break
		}
	}

	// Once connected, spawn go routine for upload?

	// Upload: Need to populate upload type. Name, data, hash.
	// Could save upload type to diskqueue.
}

func main() {
	var cfg config

	fmt.Println("Build Version:", version)
	fmt.Println("Build Commit: ", commit)
	fmt.Println("Build Stamp:  ", stamp)

	if len(os.Args) < 2 {
		log.Fatal("Usage: iris-client <config file>")
	}

	_, err := toml.DecodeFile(os.Args[1], &cfg)
	if err != nil {
		log.Fatal("Error reading config:", err)
	}

	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})

	// Create folder
	err = os.MkdirAll(filepath.Dir(cfg.Queue), 0755)
	if err != nil {
		log.Fatal(err)
	}

	// Expand optional environment varaibles
	// cfg.FromPath = os.ExpandEnv(cfg.FromPath)
	// cfg.ToPath = os.ExpandEnv(cfg.ToPath)
	// cfg.ArchivePath = os.ExpandEnv(cfg.ArchivePath)

	log.Println("Opening disk queue:", cfg.Queue)
	dq, err := iris.NewDiskQueue(cfg.Queue)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Opening CAN socket:", cfg.Device)
	sock, err := iris.NewCANSocket(cfg.Device, cfg.Filters)
	if err != nil {
		log.Fatal(err)
	}

	// conf, err := iris.LoadClientConfig(cfg.CACrt, cfg.ClientCrt, cfg.ClientKey)
	// if err != nil {
	// 	log.Println("Could not read key file")
	// 	return
	// }

	// c, err := iris.Connect(conf, addr)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// err = c.Upload(os.Args[1], os.Args[1])
	// if err != nil {
	// 	panic(err)
	// }

	// Main operation
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	done := make(chan bool, 1)

	msg := make(chan iris.CANMsg)

	go encodeCANMsgs(dq, msg, sig, done)
	go readCANMsgs(sock, msg)

	<-done
}

type logWriter struct {
	io.Writer
	name string
}

func NewLogWriter(w io.Writer, name string) *logWriter {
	return &logWriter{Writer: w, name: name}
}

func (lw *logWriter) Write(p []byte) (nn int, err error) {
	log.Println(lw.name, ": Writing", len(p), "bytes")
	return lw.Writer.Write(p)
}
