package iris

import (
	"hash/crc32"
)

//go:generate msgp

type StreamType byte

const (
	Raw StreamType = iota
	Tcp
	Udp
	Gob
	Msgp
	Pty
)

var streamTypes = [...]string{
	"raw",
	"tcp",
	"udp",
	"gob",
	"msgp",
	"pty",
}

type upload struct {
	Name string `msg:"name"`
	Data []byte `msg:"data"`
	Hash uint32 `msg:"hash"`
}

func (s StreamType) String() string { return streamTypes[s] }

// func (Message) Marshal{}

var crc32c *crc32.Table

func init() {
	crc32c = crc32.MakeTable(crc32.Castagnoli)
}
