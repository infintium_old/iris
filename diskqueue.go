package iris

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

type int64slice []int64

func (s int64slice) Search(x int64) int {
	return sort.Search(len(s), func(i int) bool {
		return s[i] >= x
	})
}

func (s int64slice) Insert(x int64) int64slice {
	i := s.Search(x)
	if i == len(s) {
		return append(s, x)
	}

	if s[i] == x {
		return s
	}

	s = append(s, 0)
	copy(s[i+1:], s[i:])
	s[i] = x
	return s
}

type DiskQueue struct {
	folder     string
	timestamps int64slice
	head       *os.File
}

func NewDiskQueue(folder string) (*DiskQueue, error) {
	files, err := filepath.Glob(filepath.Join(folder, "*.ggc"))
	if err != nil {
		return nil, err
	}

	var dq DiskQueue

	dq.folder = folder

	for _, f := range files {
		i, err := fileToInt(f)
		if err == nil {
			fmt.Println("Found:", f, i)
			dq.timestamps = dq.timestamps.Insert(i)
		}
	}

	fmt.Println("Queue:", dq.timestamps)

	return &dq, nil
}

func (dq *DiskQueue) Write(p []byte) (nn int, err error) {
	i := time.Now().Unix()

	f := intToFile(dq.folder, i)

	file, err := os.OpenFile(f, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return
	}

	nn, err = file.Write(p)
	if err != nil {
		return
	}

	fmt.Println("Wrote", nn, "bytes to", f)

	dq.timestamps = dq.timestamps.Insert(i)

	fmt.Println("Queue:", dq.timestamps)

	err = file.Close()

	return
}

func (dq *DiskQueue) Read(p []byte) (n int, err error) {

	// Check if file is open
	if dq.head == nil {
		// Check if we have a file ready
		if len(dq.timestamps) > 0 {
			f := intToFile(dq.folder, dq.timestamps[0])

			dq.head, err = os.Open(f)
		} else {
			return 0, io.EOF
		}
	}

	// Do we have an old file to delete?

	// We should have an open file. Read len(p) bytes
	n, err = dq.head.Read(p)

	if err == io.EOF {
		f := dq.head.Name()

		dq.head.Close()

		// Check for next file and open it.

		// Delete file?
		os.Remove(f)
	}

	return
}

func fileToInt(filename string) (i int64, err error) {
	b := filepath.Base(filename)
	s := strings.TrimSuffix(b, filepath.Ext(b))
	i, err = strconv.ParseInt(s, 10, 64)
	return
}

func intToFile(folder string, i int64) string {
	return filepath.Join(folder, fmt.Sprintf("%019d.ggc", i))
}
