package iris

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
)

type Server struct {
	Addr      *net.TCPAddr
	TLSConfig *tls.Config
	DB        map[string]*Batch
	SockPath  string
	mux       SessionMux
	listener  net.Listener
	sessions  map[net.Addr]*Session
}

type tlsHandshakeTimeoutError struct{}

func (tlsHandshakeTimeoutError) Timeout() bool   { return true }
func (tlsHandshakeTimeoutError) Temporary() bool { return true }
func (tlsHandshakeTimeoutError) Error() string   { return "net/http: TLS handshake timeout" }

func (srv *Server) MuxListen(t StreamType) net.Listener {
	m := muxListener{addr: srv.Addr}

	m.c = make(chan net.Conn)

	if srv.mux.listeners == nil {
		srv.mux.listeners = make(map[StreamType]muxListener)
	}
	srv.mux.listeners[t] = m

	return &m
}

func (srv *Server) ListenAndServe() error {
	var err error

	// srv.listener, err = quic.ListenAddr(srv.Addr, srv.TLSConfig, nil)
	// srv.listener, err = tls.Listen("tcp", srv.Addr, srv.TLSConfig)
	srv.listener, err = net.ListenTCP("tcp", srv.Addr)
	if err != nil {
		return err
	}

	defer srv.listener.Close()

	log.Info("Listening on ", srv.Addr)

	return srv.ServeTLS()
}

func (srv *Server) ServeTLS() error {
	srv.sessions = make(map[net.Addr]*Session)
	tlsListener := tls.NewListener(srv.listener, srv.TLSConfig)

	newConns := make(chan net.Conn)

	go func(l net.Listener) {
		for {
			c, err := l.Accept()
			if err != nil {
				// handle error (and then for example indicate acceptor is down)
				newConns <- nil
				return
			}
			newConns <- c
		}
	}(tlsListener)

	for {
		select {
		case conn := <-newConns:
			go func() {
				if conn == nil {
					// TODO: Return error here
					return
				}

				le := log.WithField("addr", conn.RemoteAddr())
				le.Info("Incoming connection")

				tlscon, ok := conn.(*tls.Conn)
				if !ok {
					log.Error("Could not convert conn to tls.Conn")
					conn.Close()
					return
				}

				le.Debug("tlscon: ok=true")

				err := tlsHandshakeTimeout(tlscon, 10*time.Second)

				if err != nil {
					le.Error("Handshake error ", err)
					conn.Close()
					return
				}

				cert := tlscon.ConnectionState().PeerCertificates[0]
				sn := fmt.Sprintf("%X", cert.SerialNumber)
				le = le.WithField("name", cert.Subject.CommonName)
				le.Info("SN: ", sn)
				le.Debug("Sig Algo: ", cert.SignatureAlgorithm.String())
				le.Debug("Pub Key Algo: ", cert.PublicKeyAlgorithm.String())
				le.Debugf("Cipher: %x", tlscon.ConnectionState().CipherSuite)

				// FIXME: This will crash if SN is not in map
				// TODO: Test IPV6 addresses
				ip, _, err := net.SplitHostPort(conn.RemoteAddr().String())
				if err != nil {
					ip = "0.0.0.0"
				}
				srv.DB[sn].UpdateTag("ip", ip)

				stat := NewStats(conn, srv.DB[sn])

				s, err := newServerSession(stat, &srv.mux, le)
				if err != nil {
					le.Error("Could not establish mux session: ", err)
					stat.Close()
					return
				}

				sock := filepath.Join(srv.SockPath, cert.Subject.CommonName+"_"+sn+".sock")
				go s.OpenPtyStream(sock, le)

				// FIXME: This probably won't work. Use serialnumber of cert?
				srv.sessions[conn.RemoteAddr()] = s
			}()
		}

	}
}

func (srv *Server) Close(le *log.Entry) error {
	for _, s := range srv.sessions {
		s.Close(le)
	}
	return srv.listener.Close()
}

func tlsHandshakeTimeout(tlsConn *tls.Conn, TLSHandshakeTimeout time.Duration) error {
	errc := make(chan error, 2)
	var timer *time.Timer // for canceling TLS handshake

	if d := TLSHandshakeTimeout; d != 0 {
		timer = time.AfterFunc(d, func() {
			errc <- tlsHandshakeTimeoutError{}
		})
	}

	go func() {
		err := tlsConn.Handshake()
		if timer != nil {
			timer.Stop()
		}

		errc <- err

	}()

	if err := <-errc; err != nil {
		return err
	}

	return nil
}

func LoadServerConfig(caFile, certFile, keyFile, crlFile string) (*tls.Config, error) {
	caCertPEM, err := ioutil.ReadFile(caFile)
	if err != nil {
		return nil, err
	}

	clients := x509.NewCertPool()
	ok := clients.AppendCertsFromPEM(caCertPEM)
	if !ok {
		log.Fatal("Failed to parse root certificate")
	}

	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}

	crlPEM, err := ioutil.ReadFile(crlFile)
	if err != nil {
		return nil, err
	}

	crl, err := x509.ParseCRL(crlPEM)
	if err != nil {
		return nil, err
	}

	verifyCRL := func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
		log.Info("Checking revoke list")

		for _, revoked := range crl.TBSCertList.RevokedCertificates {
			if verifiedChains[0][0].SerialNumber.Cmp(revoked.SerialNumber) == 0 {
				log.Info("Certificate is revoked")
				return errors.New("Certificate revoked")
			}
		}

		return nil
	}

	return &tls.Config{
		Certificates:          []tls.Certificate{cert},
		VerifyPeerCertificate: verifyCRL,
		ClientAuth:            tls.RequireAndVerifyClientCert, // Verify clients (mutual auth)
		ClientCAs:             clients,                        // Use our own CAs
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256}, // Reject clients without proper cipher
		MinVersion: tls.VersionTLS12, // Don't use previous TLS versions
	}, nil
}
