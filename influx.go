package iris

import (
	"fmt"
	"time"

	"github.com/influxdata/influxdb/client/v2"
)

// Influx writes are faster using batches that have the same
// database, precision, retention policy.  It should also be
// faster to batch the same measurement and tags.  Tags should
// be sorted (client performs this in NewPoint()).
type Batch struct {
	client    client.Client
	bpc       client.BatchPointsConfig
	name      string
	tags      map[string]string
	batchSize int
	timer     *time.Timer
	bp        client.BatchPoints
}

func NewBatch(c client.Client, db, precision, name string, tags map[string]string, size int) (*Batch, error) {
	// Create database (will not overwrite existing database)
	_, err := query(c, db, precision, "CREATE DATABASE %s", db)
	if err != nil {
		return nil, err
	}

	b := &Batch{
		client: c,
		bpc: client.BatchPointsConfig{
			Database:  db,
			Precision: precision,
		},
		name:      name,
		tags:      tags,
		batchSize: size,
	}

	bp, err := client.NewBatchPoints(b.bpc)
	if err != nil {
		return nil, err
	}

	b.bp = bp

	return b, nil
}

func (b *Batch) UpdateTag(k, v string) {
	b.tags[k] = v
}

func (b *Batch) Write(fields map[string]interface{}, t time.Time) error {
	pt, err := client.NewPoint(b.name, b.tags, fields, t)
	if err != nil {
		return err
	}

	b.bp.AddPoint(pt)

	if len(b.bp.Points()) > b.batchSize {
		return b.Flush()
	}

	return nil
}

func (b *Batch) Flush() error {
	if len(b.bp.Points()) > 0 {
		err := b.client.Write(b.bp)
		if err != nil {
			return err
		}

		bp, err := client.NewBatchPoints(b.bpc)
		if err != nil {
			return err
		}

		b.bp = bp
	}
	return nil
}

func query(c client.Client, db, precision, format string, a ...interface{}) (res *client.Response, err error) {
	qs := fmt.Sprintf(format, a...)

	// defer timeElapsed(time.Now(), qs)

	q := client.NewQuery(qs, db, precision)
	// q.Chunked = true
	response, err := c.Query(q)
	if err == nil {
		if response.Error() != nil {
			return res, response.Error()
		}
	} else {
		return response, err
	}
	return response, nil
}
