package iris

import (
	"io"
	"sync/atomic"
	"time"

	log "github.com/sirupsen/logrus"
)

type stats struct {
	rwc    io.ReadWriteCloser
	in     int64
	out    int64
	batch  *Batch
	ticker *time.Ticker
}

func NewStats(rwc io.ReadWriteCloser, batch *Batch) *stats {
	s := stats{rwc: rwc, batch: batch}

	fields := map[string]interface{}{"connected": 1, "bytes_in": s.in, "bytes_out": s.out}
	s.batch.Write(fields, time.Now())
	s.batch.Flush()

	s.ticker = time.NewTicker(10 * time.Second)

	go func() {
		tickChan := s.ticker.C

		for {
			select {
			case <-tickChan:
				fields := map[string]interface{}{"bytes_in": atomic.LoadInt64(&s.in), "bytes_out": atomic.LoadInt64(&s.out)}
				err := s.batch.Write(fields, time.Now())
				if err != nil {
					log.Error("Error writing stats: ", err)
				}
				err = s.batch.Flush()
				if err != nil {
					log.Error("Error flushing stats: ", err)
				}
			}
		}
	}()

	return &s
}

func (r *stats) Read(p []byte) (n int, err error) {
	n, err = r.rwc.Read(p)
	atomic.AddInt64(&r.in, int64(n))
	return
}

func (w *stats) Write(p []byte) (n int, err error) {
	n, err = w.rwc.Write(p)
	atomic.AddInt64(&w.out, int64(n))
	return
}

func (s *stats) Close() error {
	log.Debug("Closing stats")

	s.ticker.Stop()

	fields := map[string]interface{}{"connected": 0, "bytes_in": s.in, "bytes_out": s.out}
	err := s.batch.Write(fields, time.Now())
	if err != nil {
		log.Error("Error writing stats: ", err)
	}
	err = s.batch.Flush()
	if err != nil {
		log.Error("Error flushing stats: ", err)
	}

	return s.rwc.Close()
}
