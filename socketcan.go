package iris

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"strconv"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
)

type CANSocket struct {
	fd int
}

// CANFilter defines a filter mask from SocketCAN
type CANFilter struct {
	ID   uint32
	Mask uint32
}

// CANFrame defines a raw CAN frame from SocketCAN
type CANFrame struct {
	ID   uint32   // 32 bit CAN_ID + EFF/RTR/ERR flags
	DLC  uint8    // frame payload length in byte (0 .. 8)
	Pad  uint8    // padding
	Res0 uint8    // reserved / padding
	Res1 uint8    // reserved / padding
	Data [8]uint8 // raw data
}

const SOL_CAN_RAW = unix.SOL_CAN_BASE + unix.CAN_RAW
const CAN_RAW_FILTER = 1

func (s CANSocket) ioctlTimestamp(tv *unix.Timeval) (err error) {
	_, _, errno := unix.Syscall(
		unix.SYS_IOCTL,
		uintptr(s.fd),
		unix.SIOCGSTAMP,
		uintptr(unsafe.Pointer(tv)),
	)
	if errno != 0 {
		err = fmt.Errorf("ioctl: %v", errno)
	}
	return
}

func NewCANSocket(ifName string, filters []CANFilter) (*CANSocket, error) {
	var s CANSocket

	fd, err := unix.Socket(unix.AF_CAN, unix.SOCK_RAW, unix.CAN_RAW)
	if err != nil {
		return nil, err
	}

	s.fd = fd

	iface, err := net.InterfaceByName(ifName)
	if err != nil {
		return nil, err
	}

	addr := &unix.SockaddrCAN{Ifindex: iface.Index}
	if err = unix.Bind(s.fd, addr); err != nil {
		return nil, err
	}

	var vlen = uint(len(filters) * 8)

	// Setup CAN filters
	if vlen > 0 {
		_, _, errno := unix.Syscall6(unix.SYS_SETSOCKOPT, uintptr(s.fd), SOL_CAN_RAW, CAN_RAW_FILTER,
			uintptr(unsafe.Pointer(&filters[0])), uintptr(vlen), 0)
		if errno != 0 {
			return nil, fmt.Errorf("setsockopt: %v", errno)
		}
	}

	return &s, nil
}

func (s CANSocket) Read(b []byte) (n int, err error) {
	n, err = unix.Read(s.fd, b)
	if err != nil {
		return n, err
	}

	return n, nil
}

func (s CANSocket) ReadFrame() (*CANFrame, error) {
	b := make([]byte, 16)

	n, err := unix.Read(s.fd, b)
	if err != nil {
		return nil, err
	}
	if n <= 0 {
		return nil, errors.New("Error reading CAN socket.")
	}

	cf := new(CANFrame)

	err = cf.Decode(b)
	if err != nil {
		return nil, err
	}

	return cf, nil
}

func (s CANSocket) ReadTimestampFrame() (*CANFrame, time.Time, error) {
	b := make([]byte, 16)

	n, err := unix.Read(s.fd, b)
	if err != nil {
		return nil, time.Now(), err
	}
	if n <= 0 {
		return nil, time.Now(), errors.New("Error reading CAN socket.")
	}

	var tv unix.Timeval

	_, _, errno := unix.Syscall(unix.SYS_IOCTL, uintptr(s.fd), unix.SIOCGSTAMP,
		uintptr(unsafe.Pointer(&tv)))
	if errno != 0 {
		return nil, time.Now(), fmt.Errorf("ioctl: %v", errno)
	}

	cf := new(CANFrame)

	err = cf.Decode(b)
	if err != nil {
		return nil, time.Now(), err
	}

	return cf, time.Unix(tv.Unix()), nil
}

func (s CANSocket) GetTimestamp() (t time.Time, err error) {
	var tv unix.Timeval

	err = s.ioctlTimestamp(&tv)

	return time.Unix(tv.Unix()), nil
}

func (c *CANFrame) Decode(b []byte) error {
	buf := bytes.NewReader(b)

	return binary.Read(buf, binary.LittleEndian, c)
}

func (c CANFrame) String() string {
	return "0x" + strconv.FormatUint(uint64(c.ID), 16) +
		" " + strconv.FormatUint(uint64(c.DLC), 10) +
		" [" + strconv.FormatUint(uint64(c.Data[0]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[1]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[2]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[3]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[4]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[5]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[6]), 16) +
		" " + strconv.FormatUint(uint64(c.Data[7]), 16) + "]"
}
